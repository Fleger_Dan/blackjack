.586
.model flat, stdcall
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;includem biblioteci, si declaram ce functii vrem sa importam
includelib msvcrt.lib
extern exit: proc
extern malloc: proc
extern memset: proc
extern printf:proc

includelib canvas.lib 
extern BeginDrawing: proc
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;declaram simbolul start ca public - de acolo incepe executia
public start
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;sectiunile programului, date, respectiv cod
.data
;aici declaram date
window_title DB "BlackJack",0
area_width EQU 2000;1000=640
area_height EQU 1200;800=480
area DD 0

format DB "%d %d",13,10,0

counter DD 0 ; numara evenimentele de tip timer

arg1 EQU 8
arg2 EQU 12
arg3 EQU 16
arg4 EQU 20
retinere_index_carte DD ?
symbol_width EQU 10
symbol_height EQU 20
carte_width EQU 40
carte_height EQU 50

chenarreadyx1 EQU,1200
chenarreadyx2 EQU,1270
chenarreadyy1 EQU,400
chenarreadyy2 EQU,440


include digits.inc
include letters.inc
include picture.inc
nr_carti DD 52
suma_21_player DD 0
suma_21_dealer DD 0
suma21 DD 0
var1 DD ?
div10 DD 10
suma_pariu DD 1000
retx DD ?
rety DD ?

deplasament_carte DD 550
.code
; procedura make_text afiseaza o litera sau o cifra la coordonatele date
; arg1 - simbolul de afisat (litera sau cifra)
; arg2 - pointer la vectorul de pixeli
; arg3 - pos_x
; arg4 - pos_y
make_text proc
	push ebp
	mov ebp, esp
	pusha
	
	mov eax, [ebp+arg1] ; citim simbolul de afisat
	cmp eax, 'A'
	jl make_digit
	cmp eax, 'Z'
	jg make_digit
	sub eax, 'A'
	lea esi, letters
	jmp draw_text
make_digit:
	cmp eax, '0'
	jl make_space
	cmp eax, '9'
	jg make_space
	sub eax, '0'
	lea esi, digits
	jmp draw_text
make_space:	
	mov eax, 26 ; de la 0 pana la 25 sunt litere, 26 e space
	lea esi, letters
	
draw_text:
	mov ebx, symbol_width
	mul ebx
	mov ebx, symbol_height
	mul ebx
	add esi, eax
	mov ecx, symbol_height
bucla_simbol_linii:
	mov edi, [ebp+arg2] ; pointer la matricea de pixeli
	mov eax, [ebp+arg4] ; pointer la coord y
	add eax, symbol_height
	sub eax, ecx
	mov ebx, area_width
	mul ebx
	add eax, [ebp+arg3] ; pointer la coord x
	shl eax, 2 ; inmultim cu 4, avem un DWORD per pixel
	add edi, eax
	push ecx
	mov ecx, symbol_width
bucla_simbol_coloane:
	cmp byte ptr [esi], 0
	je simbol_pixel_alb
	mov dword ptr [edi], 0
	jmp simbol_pixel_next
simbol_pixel_alb:
	mov dword ptr [edi], 0FFFFFFh
simbol_pixel_next:
	inc esi
	add edi, 4
	loop bucla_simbol_coloane
	pop ecx
	loop bucla_simbol_linii
	popa
	mov esp, ebp
	pop ebp
	ret
make_text endp

; un macro ca sa apelam mai usor desenarea simbolului
make_text_macro macro symbol, drawArea, x, y
	push y
	push x
	push drawArea
	push symbol
	call make_text
	add esp, 16
endm

suma_proc PROC  ; calculez suma pentru player
    push EBP
	mov EBP,ESP
    pusha
	
	mov EAX,0
	mov EBX,0
	mov EBX,suma21
	mov EAX,[EBP+8]
	
	cmp EAX,50
	je A
	cmp EAX,29
	je A
	cmp EAX,28
	je A
	cmp EAX,27
	je A
	
	cmp EAX,8
	je A2
	cmp EAX,24
	je A2
	cmp EAX,34
	je A2
	cmp EAX,36
	je A2
	
	cmp EAX,1
	je A3
	cmp EAX,25
	je A3
	cmp EAX,26
	je A3
	cmp EAX,44
	je A3
	
	cmp EAX,3
	je A4
	cmp EAX,21
	je A4
	cmp EAX,43
	je A4
	cmp EAX,45
	je A4
	
	cmp EAX,10
	je A5
	cmp EAX,11
	je A5
	cmp EAX,31
	je A5
	cmp EAX,47
	je A5
	
	cmp EAX,13
	je A6
	cmp EAX,17
	je A6
	cmp EAX,37
	je A6
	cmp EAX,42
	je A6
	
	cmp EAX,2
	je A7
	cmp EAX,30
	je A7
	cmp EAX,39
	je A7
	cmp EAX,4
	je A7
	
	cmp EAX,33
	je A8
	cmp EAX,23
	je A8
	cmp EAX,38
	je A8
	cmp EAX,40
	je A8
	
	cmp EAX,16
	je A9
	cmp EAX,32
	je A9
	cmp EAX,46
	je A9
	cmp EAX,51
	je A9
	
	cmp EAX,14 ; 10
	je A10
	cmp EAX,15
	je A10
	cmp EAX,18
	je A10
	cmp EAX,48
	je A10
	
	cmp EAX,19  ;j 
	je A10
	cmp EAX,20
	je A10
	cmp EAX,22
	je A10
	cmp EAX,35
	je A10
	
	cmp EAX,0    ;q
	je A10
	cmp EAX,6
	je A10
	cmp EAX,7
	je A10
	cmp EAX,41
	je A10
	
	cmp EAX,5    ;k
	je A10
	cmp EAX,9
	je A10
	cmp EAX,12
	je A10
	cmp EAX,49
	je A10

	A:
	add EBX,11
	jmp iesire
	A2:
	add EBX,2
	jmp iesire
	A3:
	add EBX,3
	jmp iesire
	A4:
	add EBX,4
	jmp iesire
	A5:
	add EBX,5
	jmp iesire
	A6:
	add EBX,6
	jmp iesire
	A7:
	add EBX,7
	jmp iesire
	A8:
	add EBX,8
	jmp iesire
	A9:
	add EBX,9
	jmp iesire
	A10:
	add EBX,10
	
	iesire:
	mov suma21,EBX
	popa
	mov ESP,EBP
	pop EBP
	
    ret
suma_proc ENDP

suma_index MACRO index
     push index
	 call suma_proc
	 add ESP,4
	 
ENDM

casutaproc PROC
    
	push EBP
	mov EBP,ESP
    pusha

	;push [EBP+12]
	;push 3
	;push offset format
    ;call printf
	;add esp,12
	
	mov EBX,0
	mov EBX,[EBP+8]
	cmp EBX,1200
	jl revenire
	cmp EBX,1270
	jg revenire
	mov EBX,[EBP+12]
	cmp EBX,360
	jl revenire 
	cmp EBX,400
	jg revenire
	
	mov suma21,0
	repetare:
	add deplasament_carte,50
	mov EAX,200
	mov EBX,area_width
	mul EBX
	add EAX,deplasament_carte
	mov EBX,4
	mul EBX
	add EAX,area
	
	mov ECX,EAX
	RDTSC
	mov EDX,0
	div nr_carti
	suma_index EDX
	mov EBX,suma21
	mov suma_21_dealer,EBX
	
	mov EAX,EDX
	mov EDX,8000
	mul EDX
	mov EDX,EAX
	mov EAX,ECX
	lea ESI,picture
	mov EBX,0
	linie_verticala1:
    mov ECX,0
	linie_orizontala123:
	add EAX,4
	mov var1,EDX
	mov EDX,[ESI+EDX]
	mov dword ptr[EAX],EDX
	mov EDX,var1
	add EDX,4
	inc ecx
	CMP ecx,40
	jne linie_orizontala123
	
	add eax,area_width*4
    add EAX,4
	
	mov ECX,0
	linie_orizontala12:
	sub EAX,4
	;mov dword ptr[EAX],ESI
	inc ecx
	CMP ecx,40
	jne linie_orizontala12
	
	;add eax,area_width*4
    sub EAX,4
	
	inc EBX
	cmp EBX,50
	jne linie_verticala1
	;generarea primei carti a dealer-ului
	mov EBX,0
	mov EBX,suma_21_dealer
	cmp EBX,10
	jle repetare
    mov suma21,0
	
	mov deplasament_carte,550
	
	push suma_21_dealer
	push suma_21_player
	push offset format
	call printf
	add esp,12
	
	mov EBX,suma_21_dealer
	cmp EBX,21
	jg castigat
	cmp suma_21_player,21
	jg pierdut
	jmp next1
	pierdut:
	make_text_macro "P",area,600,500
	make_text_macro "I",area,610,500
	make_text_macro "E",area,620,500
	make_text_macro "R",area,630,500
	make_text_macro "D",area,640,500
	make_text_macro "U",area,650,500
	make_text_macro "T",area,660,500
	sub suma_pariu,20
	jmp revenire
	next1:
	cmp EBX,suma_21_player
	je remiza
	jmp next2
	remiza:
	make_text_macro "R",area,600,500
	make_text_macro "E",area,610,500
	make_text_macro "M",area,620,500
	make_text_macro "I",area,630,500
	make_text_macro "Z",area,640,500
	make_text_macro "A",area,650,500
	
    jmp revenire
	next2:
	cmp EBX,suma_21_player
	jg pierdut1
	jmp next3
	pierdut1:
	make_text_macro "P",area,600,500
	make_text_macro "I",area,610,500
	make_text_macro "E",area,620,500
	make_text_macro "R",area,630,500
	make_text_macro "D",area,640,500
	make_text_macro "U",area,650,500
	make_text_macro "T",area,660,500
	sub suma_pariu,20
	jmp revenire
	next3:
	cmp EBX,suma_21_player
	jl castigat
	jmp revenire
	castigat:
	make_text_macro "C",area,600,500
	make_text_macro "A",area,610,500
	make_text_macro "S",area,620,500
	make_text_macro "T",area,630,500
	make_text_macro "I",area,640,500
	make_text_macro "G",area,650,500
	make_text_macro "A",area,660,500
	make_text_macro "T",area,670,500
	add suma_pariu,20
	revenire:
	popa
	mov ESP,EBP
	pop EBP
	
	ret
casutaproc ENDP

casuta MACRO xb,yb
       push yb
	   push xb
	   call casutaproc
       add esp,8
	 
	
ENDM

resetproc PROC
    
	push EBP
	mov EBP,ESP
    pusha

	;push [EBP+12]
	;push 3
	;push offset format
    ;call printf
	;add esp,12
	
	mov EBX,0
	mov EBX,[EBP+8]
	cmp EBX,1200
	jl revenire1
	cmp EBX,1270
	jg revenire1
	mov EBX,[EBP+12]
	cmp EBX,460
	jl revenire1 
	cmp EBX,500
	jg revenire1
	
	mov suma_21_player,0
	mov suma21,0
	
	;reset mesaj
	mov eax,500
	mov EBX,area_width
	mul ebx
	add eax,580
	mov ebx,4
	mul ebx
	add eax,area
	
	mov EBX,0
	resetjos:
    mov ECX,0
	resetdreapta:
	add EAX,4
	mov dword ptr[EAX],0H
	inc ecx
	CMP ecx,100
	jne resetdreapta
	
	add eax,area_width*4
    add EAX,4
	
	mov ECX,0
	resetdreapta1:
	sub EAX,4
	inc ecx
	CMP ecx,100
	jne resetdreapta1
    sub EAX,4
	
	inc EBX
	cmp EBX,30
	jne resetjos
	
	;reset dealer
	mov eax,200
	mov EBX,area_width
	mul ebx
	add eax,600
	mov ebx,4
	mul ebx
	add eax,area
	
	mov EBX,0
	resetjos1:
    mov ECX,0
	resetdreapta12:
	add EAX,4
	mov dword ptr[EAX],0H
	inc ecx
	CMP ecx,260
	jne resetdreapta12
	
	add eax,area_width*4
    add EAX,4
	
	mov ECX,0
	resetdreapta11:
	sub EAX,4
	inc ecx
	CMP ecx,260
	jne resetdreapta11
    sub EAX,4
	
	inc EBX
	cmp EBX,70
	jne resetjos1
	
    revenire1:
	popa
	mov ESP,EBP
	pop EBP
	ret
resetproc ENDP

reset MACRO xb1,yb1
       push yb1
	   push xb1
	   call resetproc
       add esp,8
	 
	
ENDM
; functia de desenare - se apeleaza la fiecare click
; sau la fiecare interval de 200ms in care nu s-a dat click
; arg1 - evt (0 - initializare, 1 - click, 2 - s-a scurs intervalul fara click)
; arg2 - x
; arg3 - y
draw proc
	push ebp
	mov ebp, esp
	pusha
	
	mov eax, [ebp+arg1]
	cmp eax, 1
	jz evt_click
	cmp eax, 2
	jz evt_timer ; nu s-a efectuat click pe nimic
	;mai jos e codul care intializeaza fereastra cu pixeli albi
	mov eax, area_width
	mov ebx, area_height
	mul ebx
	shl eax, 2
	push eax
	push 0
	push area
	call memset
	add esp, 12
	
	;CHENAR BUTON READY
	mov EAX,[EBP+arg3]
	mov EBX,area_width
	add EAX,400
	mul EBX
	add EAX,[EBP+arg2]
	add EAX,1200
	mov EBX,4
	mul EBX
	add EAX,area
	
	mov ECX,70
	buton_dreapta:
	mov dword ptr [EAX],0000FFh
	add eax,4
	loop buton_dreapta
	
	mov ECX,40
	buton_jos:
	mov dword ptr [EAX],0000FFh
	sub eax,area_width*4
	loop buton_jos
	
	mov ECX,70
	buton_stanga:
	mov dword ptr [EAX],0000FFh
	sub eax,4
	loop buton_stanga
	
	mov ECX,40
	buton_sus:
	mov dword ptr [EAX],0000FFh
	add eax,area_width*4
	loop buton_sus
	;generare buton reset
	mov EAX,500
	mov EBX,area_width
	mul EBX
	add EAX,1200
	
	mov EBX,4
	mul EBX
	add EAX,area
	
	mov ECX,70
	buton_dreapta1:
	mov dword ptr [EAX],0000FFh
	add eax,4
	loop buton_dreapta1
	
	mov ECX,40
	buton_jos1:
	mov dword ptr [EAX],0000FFh
	sub eax,area_width*4
	loop buton_jos1
	
	mov ECX,70
	buton_stanga1:
	mov dword ptr [EAX],0000FFh
	sub eax,4
	loop buton_stanga1
	
	mov ECX,40
	buton_sus1:
	mov dword ptr [EAX],0000FFh
	add eax,area_width*4
	loop buton_sus1
	
	;chenar dealer
	mov EAX,275
	mov EBX,area_width
	mul EBX
	add EAX,580
	
	mov EBX,4
	mul EBX
	add EAX,area
	
	mov ECX,300
	buton_dreapta2:
	mov dword ptr [EAX],0000FFh
	add eax,4
	loop buton_dreapta2
	
	mov ECX,100
	buton_jos2:
	mov dword ptr [EAX],0000FFh
	sub eax,area_width*4
	loop buton_jos2
	
	mov ECX,300
	buton_stanga2:
	mov dword ptr [EAX],0000FFh
	sub eax,4
	loop buton_stanga2
	
	mov ECX,100
	buton_sus2:
	mov dword ptr [EAX],0000FFh
	add eax,area_width*4
	loop buton_sus2
	
	;chenar player
	mov EAX,360
	mov EBX,area_width
	mul EBX
	add EAX,580
	
	mov EBX,4
	mul EBX
	add EAX,area
	
	mov ECX,80
	buton_dreapta3:
	mov dword ptr [EAX],0000FFh
	add eax,4
	loop buton_dreapta3
	
	mov ECX,70
	buton_jos3:
	mov dword ptr [EAX],0000FFh
	sub eax,area_width*4
	loop buton_jos3
	
	mov ECX,80
	buton_stanga3:
	mov dword ptr [EAX],0000FFh
	sub eax,4
	loop buton_stanga3
	
	mov ECX,70
	buton_sus3:
	mov dword ptr [EAX],0000FFh
	add eax,area_width*4
	loop buton_sus3
	
	;chenar mesaj
	mov EAX,530
	mov EBX,area_width
	mul EBX
	add EAX,580
	
	mov EBX,4
	mul EBX
	add EAX,area
	
	mov ECX,120
	buton_dreapta4:
	mov dword ptr [EAX],0000FFh
	add eax,4
	loop buton_dreapta4
	
	mov ECX,40
	buton_jos4:
	mov dword ptr [EAX],0000FFh
	sub eax,area_width*4
	loop buton_jos4
	
	mov ECX,120
	buton_stanga4:
	mov dword ptr [EAX],0000FFh
	sub eax,4
	loop buton_stanga4
	
	mov ECX,40
	buton_sus4:
	mov dword ptr [EAX],0000FFh
	add eax,area_width*4
	loop buton_sus4
	
	jmp afisare_litere
	
evt_click:
    casuta [EBP+arg2],[EBP+arg3]
	reset  [EBP+arg2],[EBP+arg3]
    ;mov EAX,[EBP+arg3]
	mov EAX,300
	mov EBX,area_width
	mul EBX
	;add EAX,[EBP+arg2]
	add EAX,600
	mov EBX,4
	mul EBX
	add EAX,area
	;calculez pozitia
    mov ECX,EAX
	RDTSC
	mov EDX,0
	div nr_carti
	mov retinere_index_carte,EDX
	suma_index retinere_index_carte
	mov EBX,suma21
	mov suma_21_player,EBX
	;AND EAX,3FH
	;cmp EAX,51
	;jg da
	;jmp revenire
	;da:
	;mov EAX,51
	;revenire:
	
	mov EAX,retinere_index_carte
	mov EDX,8000
	mul EDX
	mov EDX,EAX
	mov EAX,ECX
	lea ESI,picture
	mov EBX,0
	linie_verticala:
    mov ECX,0
	linie_orizontala:
	add EAX,4
	mov var1,EDX
	mov EDX,[ESI+EDX]
	mov dword ptr[EAX],EDX
	mov EDX,var1
	add EDX,4
	inc ecx
	CMP ecx,40
	jne linie_orizontala
	
	add eax,area_width*4
    add EAX,4
	
	mov ECX,0
	linie_orizontala1:
	sub EAX,4
	;mov dword ptr[EAX],ESI
	inc ecx
	CMP ecx,40
	jne linie_orizontala1
	
	;add eax,area_width*4
    sub EAX,4
	
	inc EBX
	cmp EBX,50
	jne linie_verticala
	 
	
	
	
	jmp afisare_litere
	
evt_timer:
	inc counter
	
afisare_litere:

    
	;afisam valoarea counter-ului curent (sute, zeci si unitati)
	mov ebx, 10
	mov eax, counter
	;cifra unitatilor
	mov edx, 0
	div ebx
	add edx, '0'
	make_text_macro edx, area, 30, 10
	;cifra zecilor
	mov edx, 0
	div ebx
	add edx, '0'
	make_text_macro edx, area, 20, 10
	;cifra sutelor
	mov edx, 0
	div ebx
	add edx, '0'
	make_text_macro edx, area, 10, 10
	
	 make_text_macro 'I', area, 1070, 100
	 make_text_macro 'N', area, 1080, 100
	 make_text_macro 'D', area, 1090, 100
	 make_text_macro 'E', area, 1100, 100
	 make_text_macro 'X', area, 1110, 100
	 make_text_macro 'U', area, 1120, 100
	 make_text_macro 'L', area, 1130, 100
	 
	 make_text_macro 'C', area, 1160, 100
	 make_text_macro 'A', area, 1170, 100
	 make_text_macro 'R', area, 1180, 100
	 make_text_macro 'T', area, 1190, 100
	 make_text_macro 'I', area, 1200, 100
	 make_text_macro 'I', area, 1210, 100
	 
	 make_text_macro 'E', area, 1240, 100
	 make_text_macro 'S', area, 1250, 100
	 make_text_macro 'T', area, 1260, 100
	 make_text_macro 'E', area, 1270, 100
	
	 make_text_macro 'S', area, 1240, 130
	 make_text_macro 'U', area, 1250, 130
	 make_text_macro 'M', area, 1260, 130
	 make_text_macro 'A', area, 1270, 130
	 
	mov eax,suma_21_player
	mov edx, 0
	div div10
	add edx, '0'
	make_text_macro edx,area,1310,130
	mov edx, 0
	div div10
	add edx, '0'
	make_text_macro edx,area,1300,130
	
	make_text_macro 'R', area, 1210, 370
	make_text_macro 'E', area, 1220, 370
	make_text_macro 'A', area, 1230, 370
	make_text_macro 'D', area, 1240, 370
	make_text_macro 'Y', area, 1250, 370
	
	make_text_macro 'R', area, 1210, 470
	make_text_macro 'E', area, 1220, 470
	make_text_macro 'S', area, 1230, 470
	make_text_macro 'E', area, 1240, 470
	make_text_macro 'T', area, 1250, 470
	
	mov eax,retinere_index_carte
	mov edx, 0
	div div10
	add edx, '0'
	make_text_macro edx,area,1310,100
	mov edx, 0
	div div10
	add edx, '0'
	make_text_macro edx,area,1300,100
	;scriem un mesaj
	 make_text_macro 'B', area, 110, 100
	 make_text_macro 'L', area, 120, 100
	 make_text_macro 'A', area, 130, 100
	 make_text_macro 'C', area, 140, 100
	 make_text_macro 'K', area, 150, 100
	 make_text_macro 'J', area, 160, 100
	 make_text_macro 'A', area, 170, 100
	 make_text_macro 'C', area, 180, 100
	 make_text_macro 'K', area, 190, 100
	 
	 make_text_macro 'D', area, 400, 200
	 make_text_macro 'E', area, 410, 200
	 make_text_macro 'A', area, 420, 200
	 make_text_macro 'L', area, 430, 200
	 make_text_macro 'E', area, 440, 200
	 make_text_macro 'R', area, 450, 200
	 
	 make_text_macro 'P', area, 400, 300
	 make_text_macro 'L', area, 410, 300
	 make_text_macro 'A', area, 420, 300
	 make_text_macro 'Y', area, 430, 300
	 make_text_macro 'E', area, 440, 300
	 make_text_macro 'R', area, 450, 300
	 
	 ;sumele de pariu
	 make_text_macro 'S', area, 150, 600
	 make_text_macro 'U', area, 160, 600
	 make_text_macro 'M', area, 170, 600
	 make_text_macro 'A', area, 180, 600
	
	 
	 mov eax,suma_pariu
	 mov edx,0
	 div div10
	 add EDX,'0'
	 make_text_macro EDX,area,230,600
	 mov edx,0
	 div div10
	 add EDX,'0'
	 make_text_macro EDX,area,220,600
	 mov edx,0
	 div div10
	 add EDX,'0'
	 make_text_macro EDX,area,210,600
	 mov edx,0
	 div div10
	 add EDX,'0'
	 make_text_macro EDX,area,200,600
	 
	 
	
	
	
final_draw:
	popa
	mov esp, ebp
	pop ebp
	ret
draw endp

start:
	;alocam memorie pentru zona de desenat
	mov eax, area_width
	mov ebx, area_height
	mul ebx
	shl eax, 2
	push eax
	call malloc
	add esp, 4
	mov area, eax
	;apelam functia de desenare a ferestrei
	; typedef void (*DrawFunc)(int evt, int x, int y);
	; void __cdecl BeginDrawing(const char *title, int width, int height, unsigned int *area, DrawFunc draw);
	push offset draw
	push area
	push area_height
	push area_width
	push offset window_title
	call BeginDrawing
	add esp, 20
	
	;terminarea programului
	push 0
	call exit
end start
